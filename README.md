# Weekly challenge - 2
* Given below are 8 datasets. __Can you cluster these?__
![alt text](./graph.png)
* These datasets are included in the `Data` folder
* You have to explore all the standard algorithms in ML for clustering/classification tasks
* Find the best algorithm which works for each dataset.
* give a detailed classification report for all the datasets.

# Guidelines
* You have to follow the __`SACRED`__ rule in __ML__ of `splitting dataset into training data and testing data.`
* The model that gives the best classification metrics using `70-30` or lower split ratio for training will be the winner.
* you have to make __your own local__ repo for doing your project.
* For submissions comment inside `submissions` issue in __THIS__ repo.
* Best Solutions will be added to the repo
