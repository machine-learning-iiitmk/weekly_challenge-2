import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

plt.style.use('seaborn')

def get_data(file_name):
    df = pd.read_csv("./Data/"+file_name,sep="\t",header=None)
    df.columns = ['x','y','label']
    return df

data_file_names = os.listdir("./Data")
data_frames = {}

for i in data_file_names:
    data_frames[i.strip('.txt')] = get_data(i)

def trim_axs(axs, N):
    """little helper to massage the axs list to have correct length..."""
    axs = axs.flat
    for ax in axs[N:]:
        ax.remove()
    return axs[:N]

fig1, axs = plt.subplots(2, 4, figsize=(20,10), constrained_layout=True)
# fig1.suptitle('can you cluster these ?',fontsize=30)
axs = trim_axs(axs, 8)
for ax,df in zip(axs,data_frames.keys()):
    groups = data_frames[df].groupby('label')
    # legend = {"plot":[],"label":[]}
    for grp in set(data_frames[df]['label']):
        grp_df = groups.get_group(grp)
        plot, = ax.plot(grp_df['x'],grp_df['y'],".")
        # legend['plot'].append(plot)
        # legend['label'].append(grp)
    # ax.legend(legend['plot'],legend['label'],loc = 'upper left')
    ax.set_ylabel('y')
    ax.set_xlabel('x')
    ax.set_title(df)
    ax.set_xticks([])
    ax.set_yticks([])

fig1.savefig('./graph.png')
# os.listdir('./')
plt.show()
